2023 Copyright LDTS Ltd. Liability Company.

The whole project is given under Creative Commons Attribution 4.0 International (CC BY 4.0) License
https://creativecommons.org/licenses/by/4.0/

Copyright and License Applicable to any part of code in this project.
